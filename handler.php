<?php 
	// We only can have access to this file using POST method
	if($_SERVER['REQUEST_METHOD'] != "POST") {
		die("Ooops. Something wrong(Only p)");
	}

	require_once("lib/autoload.inc.php"); 


	$form_data = json_decode($_POST['data']);
	
	$request_handler = new RequestHandler();
	$request_handler->handleRequest($form_data);


?>