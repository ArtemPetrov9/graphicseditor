<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Editor</title>

	<link rel="stylesheet" href="styles/style.css">
	<script   src="https://code.jquery.com/jquery-2.2.4.min.js"   integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="   crossorigin="anonymous"></script>

	<!-- BOOTSTRAP -->

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

	<!-- Latest compiled and minified JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

	<script   src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"   integrity="sha256-xNjb53/rY+WmG+4L6tTl9m6PpqknWZvRt0rO1SRnJzw="   crossorigin="anonymous"></script>
	<!-- BOOTSTRAP END -->

</head>
<body>
<input id="session-id" type="hidden" value="<?=$_COOKIE['PHPSESSID']; ?>"/>

<div class="container">

	<h1><a href="<?=$_SERVER['PHP_SELF']?>">Editor</a></h1>

    <div class="panel panel-default">
	    <div class="panel-heading">Step 1.Choose type of figure</div>
	  
	    <div class="panel-body" id="step-1">
		    <form id="figure-type-choose">
				<select name="figure_type" id="figure_type">
					<option value="point">Point</option>
					<option value="section">Section</option>
					<option value="polygon_chain">Polygon Chain</option>
					<option value="triangle">Triangle</option>
					<option value="rectangle">Rectangle</option>
					<option value="parallelogram">Parallelogram</option>
					<option value="square">Square</option>
					<option value="circle">Circle</option>
					<option value="oval">Oval</option>
					<option value="arc">Arc(дуга окружности)</option>
					<option value="text">Text</option>
				</select>    
				<input class="btn btn-default" type="submit" value="Submit"/>
		    </form>
	    </div>
	    <div class="panel panel-footer" id="step-1-result-message"></div>
    </div>

    <div class="panel panel-default">
	    <div class="panel-heading">Step 2.Choose parameters of figure</div>
	  
	    <div class="panel-body" id="step-2">
	    	<form role="form"  id="step-2-form">
	    		<input type="submit" class="btn btn-default" value="Send parameters"/>
	    	</form>
			<br />
	    	<button id="enough-pics" class="btn btn-warning">Enough pics!</button>
	    	<div id="step-2-error">Sum of all angles must be 360. Please try again</div>
	    </div>
    </div>

    <div class="panel panel-default">
	    <div class="panel-heading">Step 3.Choose format</div>
	    <div class="panel-body" id="step-3">
	    	<form role="form" id="figure-format-choose">
	    		<div class="form-group">
			    	<select class="form-control" name="figure_format">
			    		<option value="Svg">Svg</option>
			    		<option value="Jpeg">Jpeg</option>
			    		<option value="Png">Png</option>
			    	</select>
			    </div>
			    <input class="btn btn-danger" type="submit" />
	    	</form>
	    </div>
    </div>

    <div class="panel panel-default">
	    <div class="panel-heading">Step 4.Your image</div>
	    <div class="panel-body" id="step-4"></div>
    </div>    

    <div id="load"></div>

</div>

	<script src="js/jquery.json.min.js"></script>
	<script src="js/script.js"></script>
	<script src="js/ajaxHandler.js"></script>		
</body>
</html>