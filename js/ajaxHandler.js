var session_id = $("#session-id").val();

// Handle step-1
$("#figure-type-choose").on( "submit", function( event ) {
  	event.preventDefault();
  	var form = $(this).serialize();

	var formData = {
		"mode":"get_figure_form",
		"form_data": form
	}

	$.ajax( {
		url: "handler.php",
		type: "POST",
		dataType: "html",
		data: {'data':$.toJSON(formData)},
		beforeSend: function() {
			$("#step-1").slideUp("fast");
			$("#load").append("<img src=\"img/loading.gif\"/>");
		},
		success: function(data) {
			$("#load").empty();
			$("#step-2-form").prepend(data);

			$("#step-2").slideDown("fast");
		},
		error: function(){
			$("#load").empty();
			$("#load").append("Something was worng. Please try again");

			//$("#step-2").slideDown("fast");	
		}		
	});
});

// Handle step-2
$("#step-2-form").on( "submit", function( event ) {
  	event.preventDefault();
  	var form = $(this).serialize();


  	// if( $("#figure_name").val() == 'Triangle' ) {

  	// 	var isValid = validateTriangleAngles();


  	// 	if(isValid == false) {
  	// 		var angles = $(".angles");

  	// 		angles.css({"border":"1px solid #d9534f"});
  	// 		angles.effect("shake");
  			
			// $("#step-2-error").fadeIn(function() {
			// 	setTimeout(function() {
			// 		$("#step-2-error").fadeOut();
			// 	}, 2000);
			// });

			// return false;
			
  	// 	}
  	// }

	var formData = {
		"mode":"get_figure_params",
		"form_data": form
	}

	$.ajax( {
		url: "handler.php",
		type: "POST",
		dataType: "html",
		data: {'data':$.toJSON(formData)},
		beforeSend: function() {
			$("#step-2").slideUp("fast");
			$("#load").append("<img src=\"img/loading.gif\"/>");
		},
		success: function(data) {
			$('#step-2-form').empty();
			$("#load").empty();
			$("#step-1-message").empty();
			$("#step-1-result-message").append(data);

			$("#step-1").slideDown("fast");
		},
		error: function(){
			$("#load").empty();
			$("#load").append("Something was worng. Please try again");
		}		
	});
});

// Handle step-3
$("#figure-format-choose").on( "submit", function( event ) {
  	event.preventDefault();
  	var form = $(this).serialize();

	var formData = {
		"mode":"get_figure_pic",
		"form_data": form
	}

	$.ajax( {
		url: "handler.php",
		type: "POST",
		dataType: "html",
		data: {'data':$.toJSON(formData)},
		beforeSend: function() {
			$("#step-3").slideUp("fast");
			$("#load").append("<img src=\"img/loading.gif\"/>");
		},
		success: function(data) {
			$("#load").empty();
			$("#step-4").prepend(data);

			$("#step-4").slideDown("fast");
		},
		error: function(){
			$("#load").empty();
			$("#load").append("Something was worng. Please try again");
				
		}		
	});
});