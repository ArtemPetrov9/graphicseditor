// Handle step-3
$("#enough-pics").on( "click", function( event ) {
  	event.preventDefault();

	$("#step-2").slideUp("fast");
	$("#step-3").slideDown("fast");

});


function validateTriangleAngles() {

	var sum = 0;
	$('.angles').each( function(index){ 
		sum += parseInt($(this).val());
	});

	if(sum == 360) {
		return true;
	}

	return false;

}