<?php 

	require_once("autoload.inc.php");

	class Arc extends Circle 
	{

		protected $angle = null;

		function __construct($params) 
		{
			parent::__construct($params);
			$this->angle = $params['angle'];
		}
		// почему он нормально не срабатывает, если вызываем предка??
		function __get($name) 
		{
			switch ($name) {
				case 'angle':
					return $this->angle;	
				case 'x':
					return $this->x;
				case 'y':
					return $this->y;
				case 'radius':
					return $this->radius;				
				default:
					parent::__get($name);
					break;
			}
		}

	}

?>