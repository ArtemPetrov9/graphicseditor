<?php 

	require_once("autoload.inc.php");

	class Circle extends Point 
	{

		protected $radius = null;

		function __construct(array $params) 
		{
			parent::__construct($params);
			$this->radius = $params['radius'];
		}

		function __get($name)
		{
			parent::__get($name);
		}

	}

?>