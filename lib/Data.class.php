<?php 

	class Data 
	{

	static function getFormList() 
	{
		return self::$formList;
	}


	private static $formList = [

		'point' => '
			<h3>Point</h3>
			
			<p>Point has default size</p>
	
			<div class="form-group">
				<label for="x">Enter x coord (1-500)</label>
				<input id="x" class="form-control" name="x" type="number" value="20" min="1" max="500" required />
			</div>

			<div class="form-group">
				<label for="y">Enter y coord(1-500)</label>
				<input id="y" class="form-control" name="y" type="number" value="20" min="1" max="500" required />
			</div>

			<input type="hidden" name="figure_name" value="Point" />',


			'section' => '
			<h3>Section</h3>

			<div class="col col-md-6"> 
				<div class="form-group">
					<label for="x1">Enter x1 coord (1-500)</p>
					<input id="x1" class="form-control" name="x1" type="number" value="20" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="y1">Enter y1 coord (1-500)</p>
					<input id="y1" class="form-control" name="y1" type="number" value="20" min="1" max="500" required />
				</div>
			</div>

			<div class="col col-md-6"> 
				<div class="form-group">
					<label for="x2">Enter x2 coord (1-500)</p>
					<input id="x2" class="form-control" name="x2" type="number" value="20" min="1" max="500" required />
				</div>			

				<div class="form-group">
					<label for="y2">Enter y2 coord (1-500)</p>
					<input id="y2" class="form-control" name="y2" type="number" value="20" min="1" max="500" required />
				</div>
			</div>

			<input type="hidden" name="figure_name" value="Section" />
			',

		'polygon_chain' => '
			<h3>Polygon chain</h3>
			<p>You can add only 4 points</p>

			<div class="col col-md-6">
				<div class="form-group">
					<label for="x1">Enter x1 coord (1-500)</p>
					<input id="x1" class="form-control" name="x1" type="number" value="20" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="y1">Enter y1 coord (1-500)</p>
					<input id="y1" class="form-control" name="y1" type="number" value="20" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="x2">Enter x2 coord (1-500)</p>
					<input id="x2" class="form-control" name="x2" type="number" value="20" min="1" max="500" required />
				</div>				

				<div class="form-group">
					<label for="y2">Enter y2 coord (1-500)</p>
					<input id="y2" class="form-control" name="y2" type="number" value="20" min="1" max="500" required />
				</div>
			</div>

			<div class="col col-md-6">
				<div class="form-group">
					<label for="x3">Enter x3 coord (1-500)</p>
					<input id="x3" class="form-control" name="x3" type="number" value="20" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="y3">Enter x2 coord (1-500)</p>
					<input id="y3" class="form-control" name="y3" type="number" value="20" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="x4">Enter x4 coord (1-500)</p>
					<input id="x4" class="form-control" name="x4" type="number" value="20" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="y4">Enter y4 coord (1-500)</p>
					<input id="y4" class="form-control" name="y4" type="number" value="20" min="1" max="500" required />
				</div>
			</div>



			<input type="hidden" name="figure_name" value="PolygonChain" />',

		'triangle' => '
			<h3>Triangle</h3>
			<p>Input triangle coordinates</p>
			<div class="col col-md-4">
				<div class="form-group">
					<label for="x1">Input x1 (1-500)</label>
					<input id="x1" class="form-control" name="x1" type="number" value="1" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="y1">Input y1 2 (1-500)</label>
					<input id="y1" class="angles form-control" type="number" name="y1"  value="1" min="1" max="500" required />
				</div>
			</div>

			<div class="col col-md-4">
				<div class="form-group">
					<label for="x2">Input x2 (1-500)</label>
					<input id="x2" class="angles form-control" type="number" name="x2" value="1" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="y2">Input y2 (1-500)</label>
					<input id="y2" class="form-control" name="y2" type="number" value="20" min="1" max="500" required />
				</div>
			</div>

			<div class="col col-md-4">
				<div class="form-group">
					<label for="x3">Input x3 (1-500)</label>
					<input id="x3" class="form-control" name="x3" value="20" type="number" min="1" max="500" required />
				</div>

				<div class="form-group">
					<label for="y3">Input y3 (1-500)</label>
					<input id="y3" class="form-control" name="y3" value="20" type="number" min="1" max="500" required />
				</div>
			</div>

			<input id="figure_name" type="hidden" name="figure_name" value="Triangle" />',


		'rectangle' => '
			<h3>Rectangle</h3>

			<div class="form-group">
				<label for="x1">Input x coord (1-500)</label>
				<input id="x1" class="form-control" name="x1" type="number" value="1" min="1" max="500" required />
			</div>

			<div class="form-group">
				<label for="y1">Input y coord (1-500)</label>
				<input id="y1" class="angles form-control" type="number" name="y1"  value="1" min="1" max="500" required />
			</div>				

			<div class="form-group">
				<label for="side1Length">Input side 1 length (10-500)</label>
				<input  class="form-control"name="side1Length" type="number" value="20" min="10" max="500" required />
			</div>

			<div class="form-group">
				<label for="side1Length">Input side 2 length (10-500)</label>
				<input class="form-control" name="side2Length" type="number" value="20" min="10" max="500" required />
			</div>

			<input type="hidden" name="figure_name" value="Rectangle" />',

			'parallelogram' => '
			<h3>Parallelogram</h3>

			<div class="form-group">
				<label for="side1Length">Input side length (10-500)</label>
				<input class="form-control" id="side1Length" name="side1Length" type="number" value="20" min="10" max="500" required />
			</div>

			<div class="form-group">
				<label for="x1">Input x1 coord (1-500)</label>
				<input class="form-control" id="x1" name="x1" type="number" value="20" min="1" max="500" required />
			</div>

			<div class="form-group">
				<label for="y1">Input y1 coord (1-500)</label>
				<input class="form-control" id="y1" name="y1" type="number" value="20" min="1" max="500" required />
			</div>

			<div class="form-group">
				<label for="x2">Input x2 coord (1-500)</label>
				<input class="form-control" id="x2" name="x2" type="number" value="20" min="1" max="500" required />
			</div>

			<div class="form-group">
				<label for="y2">Input y2 coord (1-500)</label>
				<input class="form-control" id="y2" name="y2" type="number" value="20" min="1" max="500" required />
			</div>

			<input type="hidden" name="figure_name" value="Parallelogram" />',

		'square' => '
		<h3>Square</h3>

		<div class="form-group">
			<label for="x1">Input x coord (1-500)</label>
			<input id="x1" class="form-control" name="x1" type="number" value="1" min="1" max="500" required />
		</div>

		<div class="form-group">
			<label for="y1">Input y coord (1-500)</label>
			<input id="y1" class="angles form-control" type="number" name="y1"  value="1" min="1" max="500" required />
		</div>		

		<div class="form-group">
			<label for="side1Length">Input side length (10-500)</label>
			<input class="form-control" id="side1Length" name="side1Length" type="number" value="20" min="10" max="500" required />
		</div>

		<input type="hidden" name="figure_name" value="Square" />',

		'circle' => '
		<h3>Circle</h3>

		<div class="form-group">
			<label for="x">Enter x coord (1-500)</label>
			<input id="x" class="form-control" name="x" type="number" value="20" min="1" max="500" required />
		</div>

		<div class="form-group">
			<label for="y">Enter y coord(1-500)</label>
			<input id="y" class="form-control" name="y" type="number" value="20" min="1" max="500" required />
		</div>

		<div class="form-group">
			<label for="radius">Enter radius (10-250)</label>
			<input class="form-control" id="radius" name="radius" type="number" value="20" min="1" max="250" required />
		</div>

		<input type="hidden" name="figure_name" value="Circle" />',

		'oval' => '
		<h3>Oval</h3>

		<div class="form-group">
			<label for="x">Enter x coord (1-500)</label>
			<input id="x" class="form-control" name="x" type="number" value="20" min="1" max="500" required />
		</div>

		<div class="form-group">
			<label for="y">Enter y coord(1-500)</label>
			<input id="y" class="form-control" name="y" type="number" value="20" min="1" max="500" required />
		</div>

		<div class="form-group">
			<label for="AxisSmall">Input big axis (10-250)</label>
			<input class="form-control" id="axisBig" name="axisBig" type="number" value="20" min="10" max="250" required />
		</div>

		<div class="form-group">
			<label for="AxisSmall">Input small axis (10-250)</label>
			<input class="form-control" id="axisSmall" name="axisSmall" type="number" value="20" min="10" max="250" required />
		</div>

		<input type="hidden" name="figure_name" value="Oval" />',

		'arc' => '
		<h3>Arc</h3>

		<div class="form-group">
			<label for="x">Enter x coord (1-500)</label>
			<input id="x" class="form-control" name="x" type="number" value="20" min="1" max="500" required />
		</div>

		<div class="form-group">
			<label for="y">Enter y coord (1-500)</label>
			<input id="y" class="form-control" name="y" type="number" value="20" min="1" max="500" required />
		</div>

		<div class="form-group">
			<label for="radius">Input radius (10-250)</label>
			<input id="radius" class="form-control" name="radius" type="number" value="20" min="10" max="250" required/>
		</div>

		<div class="form-group">
			<label for="angle">Input arc angle (1-360)</label>
			<input id="angle" class="form-control" name="angle" type="number" value="20" min="1" max="360" required/>
		</div>

		<input type="hidden" name="figure_name" value="Arc" />',

		'text' => '
		<h3>Text</h3>

		<div class="form-group">
			<label for="text">Input text(only latin letters and digits, 30 max)</label>
			<input id="text" class="form-control" name="text" pattern="[a-zA-Z\d]{1,30}" required/>
		</div>

		<div class="form-group">
			<label for="textSize">Input text size (12-48)</label>
			<input id="textSize" class="form-control" type="number" name="textSize" value="14" min="12" max="48" required/>
		</div>

		<input type="hidden" name="figure_name" value="Text" />'
	];

	}

?>