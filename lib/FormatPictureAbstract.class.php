<?php

	require_once("autoload.inc.php");

	abstract class FormatPictureAbstract {

		abstract function renderPicture($data);

		protected $filename = null;

		function __construct() {
			if(!is_dir(Config::PICTURE_PATH)) {
				mkdir(Config::PICTURE_PATH);
			}			
			$this->filename = md5(uniqid());
		}

		protected function createLink($is_created, $ext) 
		{
			if($is_created) {
				return "<img src='pictures/$this->filename.$ext' alt='Somethig wrong' />";
			}
			return "Somethig wrong";
		} 

	}


?>