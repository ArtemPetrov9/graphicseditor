<?php
	
	abstract class HandlerAbstract
	{
		abstract function handleRequest($requestData);
	}

?>