<?php

	require_once("autoload.inc.php");

	class JpegPicture extends FormatPictureAbstract {

		function __construct() 
		{
			parent::__construct();
		}		

		function renderPicture($picture) 
		{
			return $this->createLink(imagejpeg($picture,"pictures/".$this->filename.".jpg"),"jpg");
		}

	}


?>