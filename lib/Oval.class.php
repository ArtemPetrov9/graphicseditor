<?php 

	require_once("autoload.inc.php");

	class Oval extends RoundedFigureAbstract 
	{

		protected $axisBig = null;
		protected $axisSmall = null;

		function __construct($params) 
		{
			parent::__construct($params);
			$this->axisBig = $params['axisBig'];
			$this->axisSmall = $params['axisSmall'];
		}
		function __get($name) 
		{
			switch ($name) {
				case 'axisBig':
					return $this->axisBig;
				case 'axisSmall':
					return $this->axisSmall;
				case 'x':
					return $this->x;
				case 'y':
					return $this->y;										
				default:
					throw new Exception("Unknown param(Rounded)");
					break;
			}
		}				

	}

?>