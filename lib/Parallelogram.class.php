<?php

	require_once("autoload.inc.php");
	
	class Parallelogram extends Rectangle 
	{

		protected $x2 = null;
		protected $y2 = null;


		function __construct($params) 
		{
			parent::__construct($params);
			$this->x2 = $params['x2'];
			$this->y2 = $params['y2'];
		}			

		function __get($name) 
		{
 			switch ($name) {
				case 'side1Length':
					return $this->side1Length;
				case 'x1':
					return $this->x1;
				case 'y1':
					return $this->y1;	
				case 'x2':
					return $this->x2;
				case 'y2':
					return $this->y2;													
				default:
					throw new Exception("Unknown property(Parallelogramm)");
			}
		}

	}
?>

