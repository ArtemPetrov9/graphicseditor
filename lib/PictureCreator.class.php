<?php

	require_once("autoload.inc.php");

	class PictureCreator
	{
		
		protected $picData = array();
		protected $format = null;
		protected $data = array();


		function __construct($format)
		{
			$this->format = $format;
			if(isset($_SESSION['picData'])) {
	 			foreach ($_SESSION['picData'] as $value) {
	 				$this->data[] = unserialize($value);
	 			}				
			} 
		}

		function createPicture() 
		{
			$pictureHtml = null;

			if(!$this->data) {
				return "No picture";
			}

			if($this->format == 'Svg' ) {
				$svgPic = new SvgPicture();
				$pictureHtml = $svgPic->renderPicture($this->data);

			} else {

				$im = imagecreatetruecolor(Config::CANVAS_WIDTH, Config::CANVAS_HEIGHT);
				$white = imagecolorallocate($im, 0xFF, 0xFF, 0xFF);			
				imagefill($im, 1, 1, $white);

				$this->generateGdPiture($im);

				if($this->format == "Png") {

					$pngPicture = new PngPicture();
					$pictureHtml = $pngPicture->renderPicture($im);

				} elseif($this->format == "Jpeg") {

					$jpegPicture = new JpegPicture();
					$pictureHtml = $jpegPicture->renderPicture($im);	

				} else {
					$pictureHtml = "Wrong format";
				}
				imagedestroy($im);
			}

			
			session_destroy();
			return $pictureHtml;
		}


		protected function generateGdPiture($image) 
		{
			$black = imagecolorallocate($image, 10, 20, 30);
			$red = imagecolorallocate($image, 250, 0, 0);
			$blue = imagecolorallocate($image, 0, 0, 255);
			$green = imagecolorallocate($image, 0, 255, 0);
			$white = imagecolorallocate($image, 255, 255, 255);

			foreach ($this->data as $value) {

				switch (get_class($value)) {
					case 'Point':
					case 'Circle':
						imageellipse ($image ,$value->x , $value->y , 5 , 5 , $green);
						break;
					case 'Oval':
						imageellipse ($image ,$value->x , $value->y , $value->axisBig , $value->axisSmall , $black);
						break;
					case 'Arc':
						// $arg = $this->describeArc($value->x, $value->y, $value->radius, 0, $value->angle);
						// $svg .= "<path fill='none' d='$arg' stroke='#446688' stroke-width='4' />";
						imagearc($image, $value->x , $value->y , $value->radius , $value->radius, 0, $value->angle, $red);
						break;
					case 'Section':
						imageline($image, $value->x1, $value->y1, $value->x2, $value->y2, $red);
						break;
					case 'PolygonChain':
						imagepolygon($image , array ($value->x1, $value->y1, $value->x2, $value->y2, $value->x3, $value->y3, $value->x4, $value->y4),4 ,$green);
						break;
					case 'Triangle':
						imagepolygon($image , array ($value->x1, $value->y1, $value->x2, $value->y2, $value->x3, $value->y3), 3, $green);
						break;
					case 'Square':
						imagerectangle($image, $value->x1, $value->y1, $value->x1 + $value->side1Length, $value->y1 + $value->side1Length, $black);
						break;
					case 'Rectangle':
						imagerectangle($image, $value->x1, $value->y1, $value->x1 + $value->side2Length, $value->y1 + $value->side1Length, $black);
						break;
					case 'Parallelogram':
						imagepolygon($image , array ($value->x1, $value->y1, $value->x2, $value->y2, $value->x2 + $value->side1Length, $value->y2, $value->x1 + $value->side1Length, $value->y1), 4, $green);
						break;					
					case 'Text':
						$font = imageloadfont("fonts/04b.gdf");
						imagestring ($image , $black, 1, 1, $value->text, $black);
						break;													
					default:
						throw new Exception("Wrong Figure(img)");
						break;



				}

			}	




		}
	} 
?>