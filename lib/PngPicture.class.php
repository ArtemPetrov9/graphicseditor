<?php

	require_once("autoload.inc.php");

	class PngPicture extends FormatPictureAbstract {

		function __construct() 
		{
			parent::__construct();
		}

		function renderPicture($picture) 
		{
			return $this->createLink(imagepng($picture,Config::PICTURE_PATH.$this->filename.".png"),"png");
		}

	}


?>