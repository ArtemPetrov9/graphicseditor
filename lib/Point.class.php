<?php 

	require_once("autoload.inc.php");

	class Point extends RoundedFigureAbstract {

		protected $radius = 5;

		function __get($name)
		{
			switch ($name) {
				case 'x':
					return $this->x;
				case 'y':
					return $this->y;
				case 'radius':
					return $this->radius;
				default:
					throw new Exception("Unknown param(Point)");
					break;
			}
		}

		function __construct(array $params) 
		{
			parent::__construct($params);
		}

	}

?>