<?php 

	require_once("autoload.inc.php");

	class PolygonChain extends Section 
	{

		protected $x3 = null;
		protected $y3 = null;
		protected $x4 = null;
		protected $y4 = null;

		function __get($name) 
		{
			switch ($name) {
				case 'x3':
					return $this->x3;
				case 'x4':
					return $this->x4;
				case 'y3':
					return $this->y3;
				case 'y4':
					return $this->y4;				
				case 'x1':
					return $this->x1;
				case 'x2':
					return $this->x2;
				case 'y1':
					return $this->y1;
				case 'y2':
					return $this->y2;							
				default:
					throw new Exception("Unknown property (Poly)");
					break;
			}
		}

		function __construct(array $params) 
		{
			parent::__construct($params);
			$this->x3 = $params['x3'];
			$this->x4 = $params['x4'];
			$this->y3 = $params['y3'];
			$this->y4 = $params['y4'];

		}

	}

?>

