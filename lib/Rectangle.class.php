<?php

	require_once("autoload.inc.php");
	
	class Rectangle extends Square {

		protected $side2Length = null;

		function __construct(array $params) {
			parent::__construct($params);
			if(isset($params['side2Length'])) {
				$this->side2Length = $params['side2Length'];
			}
			
		}			

		function __get($name) {
 			switch ($name) {
				case 'x1':
					return $this->x1;
				case 'y1':
					return $this->y1;					
				case 'side1Length':
					return $this->side1Length;
				case 'side2Length':
					return $this->side2Length;			
				default:
					throw new Exception("Unknown property(Rect)");
					break;
			}
		}

	}

?>

