<?php
	
	require_once("autoload.inc.php");

	/**
	* 
	*/
	class RequestHandler extends HandlerAbstract
	{

		/**
		 * [handleRequest description]
		 * @param  [object] $formData [description]
		 * @return [type]            [description]
		 * Function process data form ajax and print needed results
		 */		
		function handleRequest($formData)
		{
			$mode = $this->filterRequestData($formData->mode);

			$requestedDataTemp = explode("&",$formData->form_data);
			foreach ($requestedDataTemp as $value) {
				$temp = explode("=", $value);
				$requestedData[$temp[0]] = $temp[1];
			}

			switch ($mode) {
				case 'get_figure_form':
					echo $this->getFigureForm($requestedData);
					break;
				case 'get_figure_params':
					echo $this->saveFigureParams($requestedData);
					break;	
				case 'get_figure_pic':
					echo $this->getPictureLink($requestedData);
					break;
				default:
					echo "Wrong mode";
					break;
			}
		}

		/**
		 * [filterRequestData function to filtrate requested data]
		 * @param  [string] $data [Get string data]
		 * @return [string]       [Return data without tags and without spaces on boards]
		 */
		protected function filterRequestData($data)
		{
			return strip_tags(trim($data));
		}

		
		protected function getFigureForm(array $requestedData)
		{
			$formList = Data::getFormList();

			$figureType = $this->filterRequestData($requestedData['figure_type']);

			if(array_key_exists($figureType, $formList)) {
				return $formList[$figureType];
			}

			return "Wrong figure";		
		}

		protected function saveFigureParams(array $requestedData)
		{
			$figureObj = $this->classFactory($requestedData);

			$_SESSION['picData'][] = serialize($figureObj);

			return "You successfully save picture. Now you can add another";

		}


		protected function classFactory(array $requestedData)
		{
			$choiceFigure = $this->filterRequestData($requestedData['figure_name']);
			return new $choiceFigure($requestedData);

		}

		protected function getPictureLink(array $requestedData)
		{
			$picture = new PictureCreator($requestedData['figure_format']);
			return $picture->createPicture();
		}
	}
?>