<?php

	require_once("autoload.inc.php");
	
	abstract class RoundedFigureAbstract extends FigureAbstract
	{

		protected $x = null;
		protected $y = null;

		function __construct(array $params) 
		{
			$this->x = $params['x'];
			$this->y = $params['y'];
		}
	}

?>