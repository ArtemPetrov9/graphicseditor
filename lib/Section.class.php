<?php 

	require_once("autoload.inc.php");

	class Section extends SectionedFigureAbstract 
	{

		function __construct($params) 
		{
			$this->x1 = $params['x1'];
			$this->x2 = $params['x2'];
			$this->y1 = $params['y1'];
			$this->y2 = $params['y2'];
		}			

		function __get($name) 
		{
			switch ($name) {
				case 'x1':
					return $this->x1;
				case 'x2':
					return $this->x2;
				case 'y1':
					return $this->y1;
				case 'y2':
					return $this->y2;							
				default:
					throw new Exception("Unknown property (Sect)");
					break;
			}
		}
	}
?>