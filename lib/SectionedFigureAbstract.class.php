<?php

	require_once("autoload.inc.php");
	
	abstract class SectionedFigureAbstract extends FigureAbstract
	{
		protected $x1 = null;
		protected $y1 = null;
		protected $x2 = null;
		protected $y2 = null;
	}

?>