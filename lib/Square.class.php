<?php

	require_once("autoload.inc.php");
	
	class Square extends FigureAbstract 
	{

		protected $side1Length = null;
		protected $x1 = null;
		protected $y1 = null;

		function __construct($params) 
		{
			$this->x1 = $params['x1'];
			$this->y1 = $params['y1'];
			$this->side1Length = $params['side1Length'];
		}			

		function __get($name) 
		{

			switch ($name) {
				case 'x1':
					return $this->x1;
				case 'y1':
					return $this->y1;	
				case 'side1Length':
					return $this->side1Length;									
				default:
					throw new Exception("Unknown property(Squ)");
					break;
			}
		}
	}

?>

