<?php

	require_once("autoload.inc.php");

	class SvgPicture extends FormatPictureAbstract 
	{


		function renderPicture($data) 
		{

			$svg = "<svg height='".Config::CANVAS_HEIGHT."'' width='".Config::CANVAS_WIDTH."'>";

			foreach ($data as $value) {

				switch (get_class($value)) {
					case 'Point':
					case 'Circle':
						$svg .= "<ellipse cx='$value->x' cy='$value->y' rx='$value->radius' ry='$value->radius' style='fill:yellow;stroke:purple;stroke-width:2' />";
						break;
					case 'Oval':
						$svg .= "<ellipse cx='$value->x' cy='$value->y' rx='$value->axisBig' ry='$value->axisSmall' style='fill:yellow;stroke:purple;stroke-width:2' />";
						break;
					case 'Arc':
						$arg = $this->describeArc($value->x, $value->y, $value->radius, 0, $value->angle);
						$svg .= "<path fill='none' d='$arg' stroke='#446688' stroke-width='4' />";
						break;
					case 'Section':
						$svg .= "<line x='$value->x1' y='$value->y1' x2='$value->x2' y2='$value->y2' style='stroke:rgb(255,0,0);stroke-width:2' />";
						break;
					case 'PolygonChain':
						$svg .= "<polyline points=\"$value->x1,$value->y1 $value->x2,$value->y2 $value->x3,$value->y3 $value->x4,$value->y4\" style=\"fill:none;stroke:black;stroke-width:2\" />";
						break;
					case 'Triangle':
						$svg .= "<path d='M $value->x1,$value->y1 L $value->x2,$value->y2 L $value->x3,$value->y3 Z' fill='none' stroke='#446688' stroke-width='1' />";
						break;
					case 'Square':
						$svg .= "<rect x='$value->x1' y='$value->y1' width='$value->side1Length' height='$value->side1Length' style='fill:rgb(0,0,255);stroke-width:3;stroke:rgb(0,0,0)' />";
						break;
					case 'Rectangle':
						$svg .= "<rect x1='$value->x1' y1='$value->y1' width='$value->side1Length' height='$value->side2Length' style='fill:rgb(0,0,255);stroke-width:3;stroke:rgb(0,0,0)' />";
						break;
					case 'Parallelogram':
						$d = $this->describeParallelogram($value->side1Length, $value->x1, $value->y1, $value->x2, $value->y2);
						$svg .= "<polygon points='$d' stroke='#446688' stroke-width='2' />";
						break;					
					case 'Text':
						$svg .= "<text x='0' y='$value->textSize' fill='black' style='font-size:$value->textSize'> $value->text </text>";
						break;													
					default:
						throw new Exception("Wrong Figure(svg)");
						break;
				}	

			}

			$svg .="</svg>";

			return $svg;

		}

		protected function describeParallelogram($side, $x1, $y1, $x2, $y2) 
		{
			return "$x1,$y1 $x2,$y2 ".($x2 + $side).",".($y2)." ".($x1+$side).",".($y1);
		}


		protected function describeArc($x, $y, $radius, $startAngle, $endAngle) 
		{
		    $start = $this->polarToCartesian($x, $y, $radius, $endAngle);
		    $end = $this->polarToCartesian($x, $y, $radius, $startAngle);

		    $arcSweep = $endAngle - $startAngle <= 180 ? "0" : "1";	

    		$d = "M ${start['x']} ${start['y']} A $radius $radius 0 $arcSweep 0 ${end['x']} ${end['y']}";    

		    return $d;       
		}

		protected function polarToCartesian($centerX, $centerY, $radius, $angleInDegrees) 
		{
		  $angleInRadians = ($angleInDegrees - 90) * M_PI / 180.0;

		  $x = $centerX + ($radius * cos($angleInRadians));
		  $y = $centerY + ($radius * sin($angleInRadians));

		  return array('x' => $x, 'y' => $y);

		}		


	}


?>