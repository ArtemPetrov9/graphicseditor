<?php

	require_once("autoload.inc.php");
	
	class Text extends FigureAbstract {

		protected $text = null;
		protected $textSize = null;

		function __construct($params) {
			$this->text = $params['text'];
			$this->textSize = $params['textSize'];
		}			

		function __get($name) {
			if($name == "text") {
				return $this->text;
			} 
			if($name == "textSize") {
				return $this->textSize;
			}
			throw new Exception("Unknown property(Txt)");
		}

	}

?>

