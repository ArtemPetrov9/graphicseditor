<?php

	require_once("autoload.inc.php");

	class Triangle extends TriangledFigureAbstract {
		protected $x1 = null;
		protected $y1 = null;

		protected $x2 = null;
		protected $y2 = null;

		protected $x3 = null;
		protected $y3 = null;

		function __construct($params) {
			$this->x1 = $params['x1'];
			$this->y1 = $params['y1'];

			$this->x2 = $params['x2'];
			$this->y2 = $params['y2'];

			$this->x3 = $params['x3'];
			$this->y3 = $params['y3'];
		}

		function __get($name) {
			switch ($name) {
				case 'x1':
					return $this->x1;
					break;
				case 'y1':
					return $this->y1;
					break;
				case 'x2':
					return $this->x2;
					break;
				case 'y2':
					return $this->y2;
					break;
				case 'x3':
					return $this->x3;
					break;
				case 'y3':
					return $this->y3;
					break;			
				default:
					throw new Exception("Wrong property(Tri)");
					break;
			}
		}

	}

?>