<?php 

	session_start();

	spl_autoload_register(function($classname) {
		require_once($classname.'.class.php');
	});

?>